module.exports = {
  purge: {
    enabled: true,
    content: [
      './src/**/*.html',
      './src/**/*.ts',
    ]
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      scale: {
        '60': '.6'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
