export type Todo = {
    id?: string
    name: string,
    description: string,
    created_at: string
}