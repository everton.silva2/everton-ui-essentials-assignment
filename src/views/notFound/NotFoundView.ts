import { View } from "../View";

const html = require('./index.html').default;
export class NotFoundView extends View {
	public getTemplate(): string {
		return html
	}

}