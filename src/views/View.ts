export abstract class View {
    public render(): void {
        const appContainer: Element = document.getElementById('app')
        appContainer.innerHTML = this.getTemplate()
    }

    protected abstract getTemplate(): string
}