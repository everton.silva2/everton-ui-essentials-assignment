import { Todo } from "../../models/Todo";
import { View } from "../View";

const html: string = require('./index.html').default;

export class RegisterTodoView extends View {
    protected getTemplate(): string {
        return html
    }

    public handleTodo(callback: Function): void {
        document.getElementById('btn-add-todo')
            .onclick = () => callback()
    }

    public fillInputFields(todo: Todo): void {
        document.querySelector("input[name='created_at']").parentElement.classList.remove('hidden')
        document.getElementById('btn-add-todo').textContent = 'Update Todo'
        Object.entries(todo).forEach(([ key, value ]: [ string, string ]) => {
            if(key === 'id') return
            (document.querySelector(`input[name='${ key }']`) as HTMLInputElement).value = value
        })
    }

    public getValuesFromFields(): any {
        const nameInput = (document.querySelector('input[name="name"') as HTMLInputElement).value
        const descriptionInput = (document.querySelector('input[name="description"') as HTMLInputElement).value
        const createdAtInput = (document.querySelector('input[name="created_at"') as HTMLInputElement)?.value
        return {
            nameInput,
            descriptionInput,
            createdAtInput
        }
    }
}