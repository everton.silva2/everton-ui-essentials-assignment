import { Todo } from "../models/Todo";
import { TodoService } from "../services/Todo.service";
import { HomeView } from "../views/home/HomeView";
import { Controller } from "./Controller";

import router from "../router";

export class HomeController implements Controller {
    constructor(
        private view: HomeView,
        private todoService: TodoService
    ) { }

    public async init(): Promise<void> {
        this.view.render()
        this.getAndListTodos()
        this.view.onClickAddTodo(this.goToAddTodo)
    }

    private getAndListTodos = (): void => {
        this.todoService.getAll().then((todos: Todo[]) => {
            this.view.renderTodoList(todos, this.onClickDeleteTodo, this.onClickEditTodo)
        })
    }

    private goToAddTodo = (): void => {
        return router.push('/register-todo')
    }

    private onClickDeleteTodo = (id: string): void => {
        this.todoService.delete(id).then(() => {
            this.getAndListTodos()
        })
    }

    private onClickEditTodo = (id: string): void => {
        router.push('/register-todo/' + id)
    }
}