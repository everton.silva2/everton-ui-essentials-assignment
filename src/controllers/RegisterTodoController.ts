import { Todo } from "../models/Todo";
import { TodoService } from "../services/Todo.service";
import { RegisterTodoView } from "../views/registerTodo/RegisterTodoView";
import { Controller } from "./Controller";

import router from "../router";

export class RegisterTodoController implements Controller {
    private currentId: string
    constructor(
        private view: RegisterTodoView,
        private todoService: TodoService
    ) {
    }

    public async init(): Promise<void> {
        this.view.render()
        this.currentId = this.getParamId()

        if(this.currentId) {
            this.view.fillInputFields(await this.todoService.getById(this.currentId))
            return this.view.handleTodo(this.updateTodo.bind(this))
        }

        this.view.handleTodo(this.addTodo.bind(this))
    }

    private addTodo = (): void => {
        const { nameInput, descriptionInput } = this.view.getValuesFromFields()
        const isValid = this.validateFields(nameInput, descriptionInput)
        if(!isValid) {
            return
        }
        const created_at = new Date().toLocaleDateString('en-US', { year: 'numeric', month: '2-digit', day: '2-digit' })
        const todo: Todo = {
            name: nameInput,
            description: descriptionInput,
            created_at
        }

        this.todoService.insert(todo).then(() => {
            router.push('/')
        })
    }

    private updateTodo = (): void => {
        const { nameInput, descriptionInput, createdAtInput } = this.view.getValuesFromFields()
        const isValid = this.validateFields(nameInput, descriptionInput)
        if(!isValid) {
            return
        }
        const todo: Todo = {
            id: this.currentId,
            name: nameInput,
            description: descriptionInput,
            created_at: createdAtInput
        }

        this.todoService.update(todo).then(() => {
            router.push('/')
        })
    }

    private validateFields = (name: string, description: string): boolean => {
        return !!name && !!description
    }

    private getParamId = (): string => {
        return location.href.match(/.*\/(\d+)$/)?.pop() || ''
    }
}