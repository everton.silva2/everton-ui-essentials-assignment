import { Controller } from "../controllers/Controller"
import { NotFoundView } from "../views/notFound/NotFoundView"

export type IRouter = {
    path: RegExp
    controller: Controller
}

export class Router {
    private routes: IRouter[]
    constructor(
        params: {
            routes: IRouter[]
        }
    ) {
        this.routes = params.routes

        window.addEventListener("popstate", () => {
            this.push(location.pathname, false)
        })

        window.addEventListener("load", () => {
            this.push(location.pathname)
        })
    }

    public push(url: string, allowPush: boolean = true) {
        const matchRoute = this.routes.some((route) => {
            if(route.path.test(url)) {
                const { controller } = route
                if(allowPush) {
                    this._push(url)
                }
                controller.init()
                return true
            }
            return false
        }) 

        if(!matchRoute) {
            new NotFoundView().render()
        }
    }

    private _push(path: string, data?: any) {
        history.pushState(data, null, window.location.origin + path)
    }
}