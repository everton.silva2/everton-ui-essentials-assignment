import { HomeController } from '../controllers/HomeController'
import { RegisterTodoController } from '../controllers/RegisterTodoController'
import { TodoService } from '../services/Todo.service'
import { HomeView } from '../views/home/HomeView'
import { RegisterTodoView } from '../views/registerTodo/RegisterTodoView'
import { Router } from './router'

const router = new Router(
	{
		routes: [
			{
				path: /^\/\/?$/,
				controller: new HomeController(
					new HomeView,
					new TodoService
				)
			},
			{
				path: /^\/register-todo\/?(\/[0-9]+)?$/,
				controller: new RegisterTodoController(
					new RegisterTodoView,
					new TodoService
				)
			}
		]
	}
)

export default router
