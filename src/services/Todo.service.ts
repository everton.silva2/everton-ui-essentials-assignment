import { Todo } from "../models/Todo";

export class TodoService {
    private urlBase: string = 'http://localhost:3000/todos'
    constructor() {}

    public getAll(): Promise<Todo[]> {
        return fetch(this.urlBase)
            .then(response => response.json())
    }

    public getById(id: string): Promise<Todo> {
        return fetch(`${ this.urlBase }/${ id }`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response.json())
    }

    public insert(todo: Todo): Promise<Todo> {
        return fetch(this.urlBase, {
            method: 'POST',
            body: JSON.stringify(todo),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response.json())
    }

    public update(todo: Todo): Promise<Todo> {
        return fetch(`${ this.urlBase }/${ todo.id }`, {
            method: 'PUT',
            body: JSON.stringify(todo),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response.json())
    }

    public delete(id: string): Promise<Todo> {
        return fetch(`${ this.urlBase }/${ id }`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response.json())
    }
}